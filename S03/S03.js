function Purchase(title, amount, isBought = false) {
    this.title = title;
    this.amount = amount;
    this.isBought = isBought;
}

// 2
function ShoppingList() {
    this.shoppingList = [];
    this.add = function(obj) {
        for(let item of this.shoppingList)
            if(item.title === obj.title) {
                item.amount++;
                return;
            }
        this.shoppingList.unshift(obj);
        return this.shoppingList[0];
    }
    this.buy = function(title) {
        for(let item of this.shoppingList) {
            if(item.title == title) {
                item.isBought = true;
            }
        }
    }
    
}
//
const shoppingList = new ShoppingList(); 
const title = document.getElementById('title');
const quantity = document.getElementById('quantity');
const list = document.getElementById('list');
const addItem = document.getElementById('add')
const buyItem = document.getElementById('buy');

addItem.addEventListener('click', function() {
    let item = new Purchase(title.value, Number(quantity.value));
    shoppingList.add(item);
    displayList();
})
buyItem.addEventListener('click', function() {
    shoppingList.buy(title.value);
    displayList();
})
function displayList() {
    while(list.firstChild) {
        list.removeChild(list.firstChild);
    }
    let l = shoppingList.shoppingList;
    l.sort(function(a, b) {return (a.isBought === b.isBought)? 0 : b.isBought? -1 : 1;})
    l.forEach(function(purchase) {
        const row = renderTableRow(purchase.title, purchase.amount, purchase.isBought)
        list.insertAdjacentHTML("beforeend", row);
    });
}

function renderTableRow(title, quantity, isBought) {
    return `<tr>
    <td>${title}</td>
    <td>${quantity}</td>
    <td>${isBought ? 'bought' : 'not bought'}</td>
    </tr>`
}
