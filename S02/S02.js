function Date1(hours, minutes, seconds) {
    this.hh = hours;
    this.mm = minutes;
    this.ss = seconds;

    this.time = function() {
        return `${this.hh}`.padStart(2, '0') + 
        `:${this.mm}`.padStart(2, '0') + 
        `:${this.ss}`.padStart(2, '0');
    }
    this.changeSeconds = function(seconds) {
        
        this.changeMinutes(Math.floor((this.ss + seconds) / 60));
        this.ss = mod((this.ss + seconds),60);
    }
    this.changeMinutes = function(minutes) {
        
        this.changeHours(Math.floor((this.mm + minutes) / 60));
        this.mm = mod((this.mm + minutes),60);
      
    }
    this.changeHours = function(hours) {
       this.hh = mod((this.hh + hours), 24);
    }
}

function mod(n, m) {
  return ((n % m) + m) % m;
}

// 
let d = new Date1(0,0,0);
let r = document.getElementById('result');
let hh = document.getElementById('hh');
let ss = document.getElementById('ss');
let mm = document.getElementById('mm');
let timeButton = document.getElementById('time');
r.innerHTML = d.time();

timeButton.addEventListener('click', function() {
    d.changeSeconds(Number(ss.value));
    d.changeMinutes(Number(mm.value));
    d.changeHours(Number(hh.value));
    r.innerHTML = d.time();
})




