// 1
function compareLength(str1, str2) {
    
    if (str1 > str2)
        return 1;
    else if (str1 < str2)
        return -1;
    else
        return 0;
}


// 2
function capitalize(str) {
    return str[0].toUpperCase() + str.substring(1).toLowerCase();
}


// 3 
function countVowel(str) {
    return str.match(/[aeiou]/gi).length ?? 0;
}


function checkSpam(str) {
    let spam = ['100% безкоштовно', 'збільшення продажів', 'тільки сьогодні', 'не видаляйте', 'ххх'];
    let lowerStr = str.toLowerCase();
    for (s of spam)
        if (lowerStr.includes(s))
            return true;
    return false;
}


// 5
function cutOff(str, maxLenght) {
    if (str.length <= maxLenght) {
        return str;
    }
    return str.substring(0, maxLenght) + '...';
}


// 6
function indexOf(str, symbol) {
    const indexes = []
    str = str.toLowerCase();
    symbol = symbol.toLowerCase();
    for(let i = 0; i < str.length; i++) {
        if(str[i] === symbol)
            indexes.push(i);
    }
    return indexes;
}