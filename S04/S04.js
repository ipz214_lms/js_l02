function Item(title, quantity, cost) {
    this.title = title;
    this.quantity = quantity;
    this.cost = cost;
}

const check = {
    itemLines : [],
    add: function(title, quantity, cost) {
        this.itemLines.push(new Item(title, quantity,cost));
    },
    sum : function (){
        return this.itemLines.reduce(
            (previousValue, currentValue) => 
            currentValue.quantity * (previousValue + currentValue.cost), 0);
    },
    costly : function(){
        return Math.max(...this.itemLines.map(item => item.cost));
    },
    average: function() {
        return this.sum() / this.itemLines.length;
    }
}


//

const title = document.getElementById('title');
const quantity = document.getElementById('quantity');
const price = document.getElementById('cost');
const checkList = document.getElementById('list');
const addButton = document.getElementById('add');

addButton.addEventListener('click', function(e) {
    check.add(title.value, Number(quantity.value), Number(price.value));
    displayCheck();
})


function displayCheck() {
    while(checkList.firstChild) {
        checkList.removeChild(checkList.firstChild);
    }
    check.itemLines.forEach(function(item) {
        checkList.insertAdjacentHTML("beforeend", renderTableRow(item.title, item.quantity, item.cost));
    })
    checkList.insertAdjacentHTML("beforeend", 
    renderTableRow(`Sum:${check.sum()}`, 
    `Av: ${check.average()}`,
    `Costly: ${check.costly()}`,
    ));
}

function renderTableRow(...text) {
    let row = `<tr>`;
    text.forEach(function(t) {
        row += `<td>${t}</td>`;
    })
    return row + `</tr>`;
}


